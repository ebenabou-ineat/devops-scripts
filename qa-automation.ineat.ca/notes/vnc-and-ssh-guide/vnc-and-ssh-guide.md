# Guide

This document contains commands, explanations, and tutorials for operating a
Linux desktop environment remotely via VNC and SSH.

As an example, this guide will refer to the `qa-automation.ineat.ca`
environment.

**Open a terminal to execute the command blocks below**

## Contents

- [Guide](#guide)
  - [Contents](#contents)
  - [Quick Start - VNC via web app (novnc)](#quick-start---vnc-via-web-app-novnc)
  - [SSH (Secure Shell)](#ssh-secure-shell)
    - [SSH Tunneling](#ssh-tunneling)
    - [X11 Forwarding](#x11-forwarding)
  - [VNC (Virtual Network Computing)](#vnc-virtual-network-computing)
    - [More information](#more-information)

## Quick Start - VNC via web app (novnc)

This quick start will show you how to connect to `qa-automation.ineat.ca` via
the _noVNC_ web VNC client.

1. **Start a VNC session on the server**

   - You can skip this step if there is already a VNC session running on the
     server.

   - Connect to the machine via SSH:

     ```bash
     ssh ineat@qa-automation.ineat.ca
     ```

   - In your SSH session:

     ```bash
     # (Optional) Kill old VNC sessions
     vncserver -kill :*

     # Start a new VNC session on display :1
     vncserver :1
     ```

   - Note that the noVNC web app is configured to connect to VNC session :1, if
     you start a VNC session on another display, you won't be able to connect
     via noVNC (but you can still connect via a different VNC client)

2. **Connect to the VNC session via the noVNC web app**

   - Navigate to <https://qa-automation.ineat.ca>

   - Click "Connect"

   - Enter your VNC password (may or may not be the same as your user password)

   - If necessary, enter your user password at the login screen

## SSH (Secure Shell)

SSH lets you connect to a terminal (a shell) on a remote machine. You can use
SSH to connect to `qa-automation.ineat.ca`.

NOTE: there is no GUI to use via normal SSH, it gives you only a terminal.

The syntax of an SSH command is: `ssh <user>@<hostname>`

For example, to connect to the `ineat` user account on `qa-automation.ineat.ca`:

```bash
ssh ineat@qa-automation.ineat.ca
```

### SSH Tunneling

To use VNC clients other than the noVNC web client, you will have to create an
SSH tunnel that maps a port on your local machine to the VNC listening port on
the server.

For example, if there is a VNC session on `:1` (running on port `5901`), then
you can create an SSH tunnel like so:

```bash
ssh ineat@qa-automation.ineat.ca -L 5901:127.0.0.1:5901
```

This will make it appear as though there is a VNC session (from the server)
running on your localhost:5901 port.

You can then connect to the session at localhost:5901 via any VNC client
application.

### X11 Forwarding

**NOTE: To use X11 forwarding, you will need a locally running X server**

Another option that is always available to run apps remotely is X11 forwarding.
With X11 forwarding, you can run Linux GUI apps on a remote server and have them
appear as if they are running on your local machine.

To use X11 forwarding, simply create an SSH session with the `-X` flag.

```bash
ssh -X ineat@qa-automation.ineat.ca
```

Then you can run a GUI app by specifying the name of the app on the command
line, like so:

```bash
firefox
```

## VNC (Virtual Network Computing)

VNC is a remote desktop software supported on most operating systems. It streams
video and inputs back and forth between a client and server.

**NOTE: for noVNC web app, it is configured to connect to the VNC session
running on port 5901**

You can control VNC using the `vncserver` utility, here are some commands:

```bash
# To list all running VNC sessions
vncserver -list

# To kill all running VNC sessions
vncserver -kill :*

# To kill a VNC session running on port 5901
vncserver -kill :1

# To kill a VNC session running on port 5902
vncserver -kill :2

# To start a VNC session
vncserver

# To start a VNC session on port 5901
vncserver :1
```

### More information

VNC links sessions to displays, and every VNC session gets its own virtual X
display. The displays are numbered in order of when the session was created,
starting from 1.

When you use the `vncserver` utility, you can refer to each display with a colon
(`:`), like so:

```bash
vncserver -kill :1
```

Each of these sessions will run on a port on the machine. The port that a
session runs on is related to the display for the session. For example, for
display `:1` the VNC session will be running on port `5901`. For session `:2`,
port `5902`. `:3` => `5903`. And so on...
