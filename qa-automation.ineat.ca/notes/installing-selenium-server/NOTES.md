# NOTES

Selenium encompasses 3 main products:

1. Selenium Web Driver
    - This is basically the "selenium" library that you are used to.
    - It is available for multiple different popular programming languages.
2. Selenium IDE
    - A browser extension that helps you create selenium scripts and test
      suites.
3. Selenium Grid
    - Sometimes referred to as Selenium Server.
    - This is basically a set of components that are meant to be ran as a
      distributed system.
    - There is a "router" which is basically an API gateway.
    - There are some queues, and finally a set of worker nodes that run the
      actual browser instances.
    - This is a totally optional piece of kit, it is not needed to run a
      centralized testing server in order to use selenium.
    - What this DOES do is speed up your tests. It allows you to run tests on
      different browsers (vendor, version, etc.) or on the same browser, but the
      tests run in parallel, rather than sequentially as with a typical test
      suite.
    - This is most likely what we will install on the new qa-automation testing
      server.

### LINKS:

- <https://www.selenium.dev/documentation/grid/configuring_components/config_help/>
    - This is the link where I would start. Basically, it might seem like kind
      of a pain to install all these Selenium Grid components (see below), but
      actually the components are quite simple to run. The entire Selenium
      Server system is packaged into a single fat jar.
    - You can even run Selenium Grid as a standalone webserver.
- <https://www.selenium.dev/documentation/grid/components_of_a_grid/>
    - Note that the diagram shown at the top of the above page just shows you
      how the different components interact - but there are actually a few
      different valid setups that you can use to run Selenium Grid. Including:
        - Standalone: Run all components together on one machine.
        - Hub + Node(s): Hub = All components except the work node, Node = a
          worker node that hosts a browser instance.
        - All components separately deployed and configured: You can, of course,
          run all the components separately and connect them together as a
          distributed system.


## Docker Stuff

<https://github.com/SeleniumHQ/docker-selenium>
<https://artifacthub.io/packages/search?ts_query_web=selenium&sort=relevance&page=1>

Check out the above for running the different components in containers.


## Instructions for setting up the Selenium Grid


