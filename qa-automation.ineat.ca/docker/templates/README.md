# NGINX config file templates

This directory contains templates for NGINX config files. The NGINX image from
DockerHub has special functionality that allows you insert env vars into your
NGINX config file.

So we are configuring NGINX partly via the environment variables injected into
the container. The container will process our template config and insert the
vars where appropriate.
